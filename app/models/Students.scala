package models

import scala.slick.driver.PostgresDriver.simple._
import play.api.Play.current
import play.api.data.Forms._
import java.util.{Date}


case class SClass(id: Option[Int] = None, sclass: String)
class SClasses(tag: Tag) extends Table[SClass](tag, "class") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def sclass = column[String]("faculty", O.NotNull)
  def * = (id.?, sclass) <> ((SClass.apply _).tupled, SClass.unapply)
}

case class Faculty(id: Option[Int] = None, faculty: String)
//type Faculty = (Int, String)
class Faculties(tag: Tag) extends Table[Faculty](tag, "faculty") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def faculty = column[String]("faculty", O.NotNull)
  def * = (id.?, faculty) <> ((Faculty.apply _).tupled, Faculty.unapply)
}

//lazy val faculties = TableQuery[Faculties]

case class Student(id: Option[Int] = None, mssv: String, name: String, email: String, bday: String, address: String, facultyId: Int, classId: Int)
class Students(tag: Tag) extends Table[Student](tag, "students") {
  // Auto Increment the id primary key column
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def mssv = column[String]("mssv", O.NotNull)
  def name = column[String]("name", O.NotNull)
  def email = column[String]("email", O.NotNull)
  def bday = column[String]("bday")
  def address = column[String]("address")
  def facultyId = column[Int]("faculty_id")
  def classId = column[Int]("class_id")
  def faculty = foreignKey("faculty", facultyId, TableQuery[Faculties])(_.id)
  def sclass = foreignKey("class", classId, TableQuery[SClasses])(_.id)
  // The name can't be null
  // the * projection (e.g. select * ...) auto-transforms the tupled
  // column values to / from a User
  def * = (id.?,mssv, name, email, bday, address, facultyId, classId) <> ((Student.apply _).tupled, Student.unapply)
}

object Students {
  val db = play.api.db.slick.DB
  val students = TableQuery[Students]

  def all: List[Student] = db.withSession { implicit session =>
    students.sortBy(_.id.asc.nullsFirst).list
  }
  def findById(id: Int): Student = db.withSession { implicit session =>
    students.filter(_.id === id).first
  }
  def findByEmail(email: String): Student = db.withSession { implicit session =>
    students.filter(_.email === email).first
  }
  def findByName(name: String): Student = db.withSession { implicit session =>
    students.filter(_.name === name).first
  }
  def create(newuser: Student) = db.withTransaction{ implicit session =>
    students += newuser
  }
  def update(updateStudent: Student) = db.withTransaction{ implicit session =>
    students.filter(_.id === updateStudent.id).update(updateStudent)
  }
  def delete(id: Int) = db.withTransaction{ implicit session =>
    students.filter(_.id === id).delete
  }

  def list(page: Int = 0, filter: String = "") : (List[Student], Int) = db.withSession { implicit session =>
    val tmp = students.sortBy(_.id.asc.nullsFirst).list.filter(x => x.name.contains(filter))
    (tmp.drop(5*page).take(5), tmp.length)
  }
}

object Faculties {
  val db = play.api.db.slick.DB
  val faculties = TableQuery[Faculties]

  def options: Map[Int,String ]= db.withSession { implicit session =>
    var A:Map[Int,String] = Map()
    faculties.sortBy(_.id.asc.nullsFirst).list.foreach(f => f.id match {
      case None =>
      case Some(x) => A+= (x -> f.faculty)
    })
    A
  }
}

object SClasses {
  val db = play.api.db.slick.DB
  val sclasses = TableQuery[SClasses]
  def options: Map[Int,String ]= db.withSession { implicit session =>
    var A:Map[Int,String] = Map()
    sclasses.sortBy(_.id.asc.nullsFirst).list.foreach(f => f.id match {
      case None =>
      case Some(x) => A+= (x -> f.sclass)
    })
    A
  }
}
