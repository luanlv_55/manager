package models

import java.sql.Date
import java.util.Calendar

import play.api.Play.current
import scala.slick.driver.PostgresDriver.simple._
import com.github.t3hnar.bcrypt._



case class Token(id: Option[Int] = None, token: String, email: String, action: String, created: String)
class Tokens(tag: Tag) extends Table[Token](tag, "tokens") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def token = column[String]("token", O.NotNull)
  def email = column[String]("email", O.NotNull)
  def action = column[String]("action", O.NotNull)
  def created = column[String]("created", O.NotNull)
  def * = (id.?, token, email, action, created) <> ((Token.apply _).tupled, Token.unapply)
}

object Tokens {
  val db = play.api.db.slick.DB
  val tokens = TableQuery[Tokens]

  def findByToken(token: String): Option[Token] = db.withSession { implicit session =>
    tokens.filter(_.token === token).firstOption
  }

  def create(email: String, action: String): Token = db.withSession { implicit session =>
    val newToken = java.util.UUID.randomUUID().toString()
    val created = Calendar.getInstance().getTime.toString
    val token = Token (None, newToken, email, action, created)
      findByEmail(email) match {
        case None => tokens += token
        case Some(token) => update(email,newToken, action)
      }
    token
  }

  def deleteByEmail(email: String) = db.withTransaction { implicit session =>
    tokens.filter(_.email === email).delete
  }

  def all: List[Token] = db.withSession { implicit session =>
    tokens.list
  }

  def findByEmail(email: String): Option[Token] = db.withSession { implicit session =>
    tokens.filter(_.email === email).firstOption
  }

  def update(email: String, token: String, action: String) = {
    db.withSession { implicit session =>
      tokens.filter(_.email === email).map(_.token).update(token)(session)
      tokens.filter(_.email === email).map(_.action).update(action)(session)
    }
  }
}

