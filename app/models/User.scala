package models

import play.api.db._
import play.api.Play.current
import scala.slick.driver.PostgresDriver.simple._
import com.github.t3hnar.bcrypt._



case class User(id: Option[Int] = None, email: String, password: String)
class Users(tag: Tag) extends Table[User](tag, "users") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def email = column[String]("email", O.NotNull)
  def password = column[String]("password", O.NotNull)
  def * = (id.?, email, password) <> ((User.apply _).tupled, User.unapply)
}

object Users {
  val db = play.api.db.slick.DB
  val users = TableQuery[Users]

  def findByEmail(email: String): Option[User] = db.withSession { implicit session =>
    users.filter(_.email === email).firstOption
  }

  def create(email: String, password: String): Either[String, User] =  db.withTransaction { implicit session =>
    findByEmail(email) match {
      case Some(_) => Left("The email has been used.")
      case None => {
        val hashed = password.bcrypt(generateSalt)
        val user = new User(None, email, hashed)
        users += user
        Right(user)
      }
   }
  }

  def deleteByEmail(email: String) = db.withTransaction { implicit session =>
    users.filter(_.email === email).delete
  }

  def authenticate(email: String, password: String): Option[User] = db.withSession { implicit session =>
    users.filter(_.email === email).firstOption match {
        case Some(user) => {
          if (password.isBcrypted(user.password)) {
            Some(user)
          } else {
            None
          }
        }
        case None => None
      }
    }

  def update(email: String, password: String): Either[String, User] = {
    findByEmail(email) match {
      case None => Left("The email has been used.")
      case Some(user) => {
        db.withSession { implicit session =>
          val hashed = password.bcrypt(generateSalt)
          users.filter(_.email === email).map(_.password)
            .update(hashed)(session)
          Right(User(user.id, email, hashed))
        }
      }
    }
  }

  def updatePassword(email: String, password: String) = db.withSession { implicit session =>
    val hashed = password.bcrypt(generateSalt)
    users.filter(_.email === email).map(_.password).update(hashed)(session)
  }
}

