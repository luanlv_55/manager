package controllers


import controllers.Authentication._
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.mvc._
import models.{Tokens, Users, Student, Students, Faculties, SClasses}

object Manager extends Controller with Secured {
  val studentForm = Form(
    mapping(
      "id" -> optional(number),
      "mssv" ->  (text verifying pattern("""^[0-9]{8}$""".r,
        error="Mã số sinh viên phải là 8 số")),
      "name" ->  (text verifying pattern("""^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]([a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]){2,24}$""".r,
        error="Tên có ít nhất 3 ký tự và không chứa ký tự đặc biệt")),
      "email" -> (text verifying pattern("""^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$""".r,
        error="Email không đúng định dạng!")),
      "bday" ->  (text verifying pattern("""^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$""".r,
        error="Ngày sinh phải là \"dd/mm/yyyy\"!")),
      "address" -> text,
      "facultyId" -> number,
      "classId" -> number
    )(Student.apply)(Student.unapply) verifying ("Email đã được dùng", result => result match {
      case x => Users.findByEmail(x.email).isEmpty && Tokens.findByEmail(x.email).isEmpty
    }))

  val studentForm2 = Form(
    mapping(
      "id" -> optional(number),
      "mssv" ->  (text verifying pattern("""^[0-9]{8}$""".r,
        error="Mã số sinh viên phải là 8 số")),
      "name" ->  (text verifying pattern("""^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]([a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]){2,24}$""".r,
        error="Tên có ít nhất 3 ký tự và không chứa ký tự đặc biệt")),
      "email" -> (text verifying pattern("""^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$""".r,
        error="Email không đúng định dạng!")),
      "bday" ->  (text verifying pattern("""^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$""".r,
        error="Ngày sinh phải là \"dd/mm/yyyy\"!")),
      "address" -> text,
      "facultyId" -> number,
      "classId" -> number
    )(Student.apply)(Student.unapply))

  def index = SecureAction { user => _ =>
    if(user == "giaovu@example.com") Ok(views.html.manager.dashboard(Students.all.length))
    else Redirect(routes.SManager.index())
  }

  def list(page: Int, filter: String = "") = Action { implicit request =>
    if(request.session.get("email").getOrElse("/") != "giaovu@example.com") Redirect(routes.Application.redirect())
    Ok(views.html.manager.list(Students.list(page, filter)._1,page , Students.list(page, filter)._2, filter, Faculties.options, SClasses.options))
  }

  def add = GetAction { implicit request =>
    if(request.session.get("email").getOrElse("/") != "giaovu@example.com") Redirect(routes.Application.redirect())
    Ok(views.html.manager.add(studentForm, Faculties.options, SClasses.options))
  }

  def edit(id:Int) = Action { implicit request =>
    if(request.session.get("email").getOrElse("/") != "giaovu@example.com") Redirect(routes.Application.redirect())
    else Ok(views.html.manager.edit(id, studentForm.fill(Students.findById(id)), Faculties.options, SClasses.options))
  }

  def update(updateid: Int) = Action {implicit request =>
    if(request.session.get("email").getOrElse("/") != "giaovu@example.com") Redirect(routes.Application.redirect())
    else {
      studentForm2.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.manager.edit(updateid, formWithErrors, Faculties.options, SClasses.options)),
        form => {
          val newuser = form.copy(id = Some(updateid))
          Students.update(newuser)
          Redirect(routes.Manager.list())
        })
    }
  }

  def delete(id: Int) = Action { implicit request =>
    if (request.session.get("email").getOrElse("/") != "giaovu@example.com") Redirect(routes.Application.redirect())
    else {
      val email = Students.findById(id).email
      Students.delete(id)
      Users.deleteByEmail(email)
      Tokens.deleteByEmail(email)
      Redirect(routes.Manager.list())
    }
  }
}
