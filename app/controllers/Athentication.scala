package controllers

import play.api._
import play.api.data.validation.Constraints._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import com.typesafe.plugin.{MailerPlugin, MailerAPI}
import org.apache.commons.mail.EmailAttachment
import models.{Student, Students, Tokens, Users, Faculties, SClasses}
import play.filters.csrf._
import scala.concurrent._

object PostAction extends ActionBuilder[Request] {
  def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
    block(request)
  }
  override def composeAction[A](action: Action[A]) = CSRFCheck(action)
}

object GetAction extends ActionBuilder[Request] {
  def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]) = {
    block(request)
  }
  override def composeAction[A](action: Action[A]) = CSRFAddToken(action)
}

trait Secured {
  //FIXME do not save email to cookie
  private def username(request: RequestHeader) = request.session.get("email")
  private def onUnauthorized(request: RequestHeader) = Results.Redirect(routes.Authentication.login).withSession("uri" -> request.uri)
  def SecureAction(f: => String => Request[AnyContent] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
    Action(request => f(user)(request))
  }
  def SecureAction[A](b: BodyParser[A])(f: => String => Request[A] => Result) = Security.Authenticated(username, onUnauthorized) { user =>
    Action(b)(request => f(user)(request))
  }
}

object Authentication extends Controller {
  val loginForm = Form(
    tuple(
      "email" -> email,
      "password" -> nonEmptyText
    ) verifying ("Email hoặc mật khẩu không đúng!", result => result match {
      case (email, password) => Users.authenticate(email, password).isDefined
    })
  )

  def login = GetAction { implicit request =>
    if(request.session.get("email").getOrElse("/") != "/") Redirect(routes.Application.redirect())
    else Ok(views.html.index(loginForm))
  }

  def authenticate = PostAction { implicit request =>
    val uri = request.session.get("uri").getOrElse("/")
    loginForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.index(formWithErrors)).flashing("error" -> "Email hoặc mật khẩu sai"),
      //FIXME do not save email to cookie
      user => Redirect(uri).withSession("email" -> user._1)
    )
  }

  def logout = GetAction { implicit request =>
      Redirect(routes.Authentication.login()).withNewSession
  }

  val signupForm = Form(
    single(
      "email" -> email
    ) verifying ("The email has been registered", result => result match {
      case(email) => Users.findByEmail(email).isEmpty
    })
  )


  import play.api.Play.current
  import com.typesafe.plugin._

  private def sendEmail(toAddress: String, subject: String, body: String) {
//    val mail =  use[MailerPlugin].email
//    mail.addFrom(Play.current.configuration.getString("smtp.user").getOrElse(""))
//    mail.addRecipient(toAddress)
//    mail.setSubject(subject)
//    mail.send(body)
    val mail: MailerAPI = play.Play.application.plugin(classOf[MailerPlugin]).email
    mail.setSubject(subject)
    mail.setRecipient(toAddress)
    mail.setFrom("somefromadd@email.com")
    //mail.addAttachment("favicon.png", new File(current.classloader.getResource("public/images/favicon.png").getPath))
    //val data: Array[Byte] = "data".getBytes
    //mail.addAttachment("data.txt", data, "text/plain", "A simple file", EmailAttachment.INLINE)
    val newBody = "Vào link sau để đặt mật khẩu:" + body
    mail.send(newBody)
  }

  private def sendEmailWithToken(toAddress: String, action: String, subject: String) = {
    val token = Tokens.create(toAddress, action)
    //val host = Play.configuration.getString("host").getOrElse("http://localhost:9000")
    val host = Play.configuration.getString("host").getOrElse("http://quanly.herokuapp.com")
    sendEmail(toAddress, subject, host + "/register/" + token.token)
  }

  val studentForm = Form(
    mapping(
      "id" -> optional(number),
      "mssv" ->  (text verifying pattern("""^[0-9]{8}$""".r,
        error="Mã số sinh viên phải là 8 số")),
      "name" ->  (text verifying pattern("""^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]([a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]){2,24}$""".r,
        error="Tên có ít nhất 3 ký tự và không chứa ký tự đặc biệt")),
      "email" -> (text verifying pattern("""^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$""".r,
        error="Email không đúng định dạng!")),
      "bday" -> text,
//    "bday" ->  (text verifying pattern("""^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$""".r,
//        error="Ngày sinh phải là \"dd/mm/yyyy\"!")),
      "address" -> text,
      "facultyId" -> number,
      "classId" -> number
    )(Student.apply)(Student.unapply) verifying ("Email đã được dùng", result => result match {
      case x => Users.findByEmail(x.email).isEmpty && Tokens.findByEmail(x.email).isEmpty
    }))

  def sendSignupEmail = PostAction { implicit request =>
    studentForm.bindFromRequest.fold(
      formWithErrors =>  BadRequest(views.html.manager.add(formWithErrors, Faculties.options, SClasses.options)),
      form => {
        val name = form.name
        val email = form.email
        //val x:(Option[Int], String, String, String, String, String) =(None, form.mssv, form.name, form.email, "", "")

        val newstudent:Student = Student(None, form.mssv, form.name, form.email, "", "", form.facultyId, form.classId)
        Students.create(newstudent)
        //Redirect(routes.Authentication.sendSignupEmail(form.email, form.name, "signup", "Welcome to website \"Quản lý luận văn cao học online\" !! "))
        sendEmailWithToken(form.email, "signup", f"Kích hoạt tài khoản $email")
        Redirect(routes.Manager.add()).flashing("success" -> f"Đã thêm học viên $name và gửi đến email $email")
      })


//    Ok("")
  }

  val registerForm = Form(
    tuple(
      "password" -> nonEmptyText,
      "repassword" -> nonEmptyText
    ) verifying(error = "Password không khớp", f => f._1 == f._2)
  )

  private def redirectToSignup = {
    BadRequest(views.html.invalid_token())
  }

  def prepareRegister(xtoken: String) = GetAction { implicit request =>
    Tokens.findByToken(xtoken) match {
      case Some(token) => {
        Ok(views.html.register(registerForm, token))
      }
      case None => redirectToSignup
    }
  }

  def register(xtoken: String) = PostAction { implicit request =>
    Tokens.findByToken(xtoken) match {
      case Some(token) => {
        registerForm.bindFromRequest.fold(
          errors => redirectToSignup,
          form => {
            token.action match {
              case "signup" => Users.create(token.email, form._1)
              case "reset" => Users.update(token.email, form._1)
            }
            //FIXME customize top page
            //FIXME do not save email to cookie
              Redirect(routes.SManager.edit(Students.findByEmail(token.email).id)).withSession("email" -> token.email)
          }
        )
      }
      case None => redirectToSignup
    }
  }

  val resetForm = Form(
    single(
      "email" -> email
    ) verifying ("The email has not been registered", result => result match {
      case(email) => Users.findByEmail(email).isDefined
    })
  )

  def prepareReset = GetAction { implicit request =>
    Ok(views.html.reset(resetForm))
  }

  def sendResetEmail = PostAction { implicit request =>
    resetForm.bindFromRequest.fold(
      formWithErrors => BadRequest(views.html.reset(formWithErrors)),
      email => {
        sendEmailWithToken(email, "reset", "Password reset")
        Redirect(routes.Authentication.login()).flashing("success" -> "Gửi email reset mật khẩu thành công!")
      }
    )
  }
}
