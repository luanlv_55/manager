package controllers

import controllers.Authentication._
import models.{Tokens, Student, Users, Students, Faculties, SClasses}
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.validation.Constraints._
import play.api.mvc.Action
import play.mvc._
import com.github.t3hnar.bcrypt._
import scala.concurrent._

object SManager extends Controller with Secured {
  val pchangeForm = Form(
    tuple(
      "email" -> text,
      "oldpassword" -> text,
      "password" -> nonEmptyText(minLength = 8),
      "repassword" -> nonEmptyText(minLength = 8)
    ).verifying(error = "Mật khẩu Cũ không đúng!", result => result match {
      case (foo, oldpassword, password, repassword) => Users.authenticate(foo, oldpassword).isDefined
    }).verifying(error = "Mật khẩu mới không trùng nhau!", f => f._3 == f._4)
  )

  val studentForm = Form(
    mapping(
      "id" -> optional(number),
      "mssv" ->  (text verifying pattern("""^[0-9]{8}$""".r,
        error="Mã số sinh viên phải là 8 số")),
      "name" ->  (text verifying pattern("""^[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]([a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]){2,24}$""".r,
        error="Tên có ít nhất 3 ký tự và không chứa ký tự đặc biệt")),
      "email" -> (text verifying pattern("""^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$""".r,
        error="Email không đúng định dạng!!")),
      "bday" ->  (text verifying pattern("""^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$""".r,
        error="Ngày sinh phải là \"dd/mm/yyyy\"!")),
      "address" -> text,
      "facultyId" -> number,
      "classId" -> number
    )(Student.apply)(Student.unapply))

  def index = SecureAction { user => _ =>
    if (user == "giaovu@example.com") Redirect(routes.Manager.index())
    else Ok(views.html.student.sdashboard())
  }

  def edit(id: Option[Int]) = GetAction {implicit request =>
    Ok(views.html.student.sedit(id, studentForm.fill(Students.findByEmail(request.session.get("email").getOrElse("/"))), Faculties.options, SClasses.options))
  }

  def update(id: Option[Int]) = PostAction { implicit request =>
    studentForm.bindFromRequest.fold(
      formWithErrors =>  BadRequest(views.html.student.sedit(id, formWithErrors, Faculties.options, SClasses.options)),
      form => {
        var id2 = id
        if (id == None) id2 = Students.findByEmail(form.email).id
        val form2:Student = Student(id2, form.mssv, form.name, form.email, form.bday, form.address, form.facultyId, form.classId)
        Students.update(form2)
        Redirect(routes.SManager.edit(id2)).flashing("success" -> "Cập nhập thông tin thành công")
      })
  }
  def changePassword = Action { implicit request =>
    val curEmail = request.session.get("email").getOrElse("/")
    Users.findByEmail(curEmail) match {
      case None => Redirect(routes.Application.redirect())
      case Some(user) => Ok(views.html.student.spassword(pchangeForm,curEmail))
    }
  }

  def updatePassword = Action { implicit request =>

    pchangeForm.bindFromRequest.fold(
      formWithErrors => {
        val curEmail = request.session.get("email").getOrElse("/")
        BadRequest(views.html.student.spassword(formWithErrors, curEmail))
      },
      form => {
        Users.updatePassword(form._1, form._3)
        Redirect(routes.SManager.changePassword()).flashing("success" -> "Đổi pass thành công!")
      }
    )

  }
}