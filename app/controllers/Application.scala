package controllers

import play.api._
import play.api.mvc.Session
import play.api.mvc._

object Application extends Controller {

  def index = Action {
    Redirect(routes.Application.redirect())
  }

  def redirect = Action { implicit request =>
    if(request.session.get("email").getOrElse("/") == "/") Redirect(routes.Authentication.login())
    else if(request.session.get("email").getOrElse("/") == "giaovu@example.com") Redirect(routes.Manager.index())
    else Redirect(routes.SManager.index())
  }
}