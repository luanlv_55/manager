# --- Sample dataset

# --- !Ups
INSERT INTO users VALUES (5, 'giaovu@example.com', '$2a$08$.b9Jdf5CO.ZQrdG5ubibq.NO5RSr9K3i9yReO/xgj3xSWwYT6yEtO');
INSERT INTO faculty VALUES (1, 'CNTT');
INSERT INTO faculty VALUES (2, 'DTVT');

INSERT INTO class VALUES (1, 'K55');
INSERT INTO class VALUES (2, 'K56');
INSERT INTO class VALUES (3, 'K57');

# --- !Downs
delete from students;
delete from tokens;
delete from users;
delete from faculty;
delete from class;
