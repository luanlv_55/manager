# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table "class" ("id" SERIAL NOT NULL PRIMARY KEY,"faculty" VARCHAR(254) NOT NULL);
create table "faculty" ("id" SERIAL NOT NULL PRIMARY KEY,"faculty" VARCHAR(254) NOT NULL);
create table "students" ("id" SERIAL NOT NULL PRIMARY KEY,"mssv" VARCHAR(254) NOT NULL,"name" VARCHAR(254) NOT NULL,"email" VARCHAR(254) NOT NULL,"bday" VARCHAR(254) NOT NULL,"address" VARCHAR(254) NOT NULL,"faculty_id" INTEGER NOT NULL,"class_id" INTEGER NOT NULL);
create table "tokens" ("id" SERIAL NOT NULL PRIMARY KEY,"token" VARCHAR(254) NOT NULL,"email" VARCHAR(254) NOT NULL,"action" VARCHAR(254) NOT NULL,"created" VARCHAR(254) NOT NULL);
create table "users" ("id" SERIAL NOT NULL PRIMARY KEY,"email" VARCHAR(254) NOT NULL,"password" VARCHAR(254) NOT NULL);
alter table "students" add constraint "class" foreign key("class_id") references "class"("id") on update NO ACTION on delete NO ACTION;
alter table "students" add constraint "faculty" foreign key("faculty_id") references "faculty"("id") on update NO ACTION on delete NO ACTION;

# --- !Downs

alter table "students" drop constraint "class";
alter table "students" drop constraint "faculty";
drop table "users";
drop table "tokens";
drop table "students";
drop table "faculty";
drop table "class";

